//
//  TaskManagement_MohammadTakbiriApp.swift
//  TaskManagement-MohammadTakbiri
//
//  Created by Mohammad Takbiri on 4/18/23.
//

import SwiftUI

@main
struct TaskManagement_MohammadTakbiriApp: App {
    var body: some Scene {
        WindowGroup {
                ListView()
        }
    }
}
