//
//  ListRowView.swift
//  TaskManagement-MohammadTakbiri
//
//  Created by Mohammad Takbiri on 4/18/23.
//

import SwiftUI

struct ListRowView: View {
    
    let task: Task
    let isCompleted: Bool?
    
    var body: some View {
        VStack{
            HStack {
                if isCompleted == nil || task.isCompleted == true {
                    Image(systemName: "checkmark.circle")
                }
                Text(task.title)
                Spacer()
            }
            HStack{
                Text(task.description)
                    .font(.system(size: 14, weight: .ultraLight))
                Spacer()
            }
        }
    }
}

struct ListRowView_Previews: PreviewProvider {
    static var previews: some View {
        ListRowView(task: Task(title: "Test", description: "Desc", isCompleted: false), isCompleted: false)
    }
}
