//
//  ListViews.swift
//  TaskManagement-MohammadTakbiri
//
//  Created by Mohammad Takbiri on 4/18/23.
//

import SwiftUI

enum TaskStatus {
    case completed
    case inProgress
    case all
}

struct ListView: View {
    
    @State private var inProgressItems: [Task] = [Task(title: "InProgress Task", description: "It's just an InProgress task", isCompleted: false)]
    
    @State private var completedItems: [Task] = [Task(title: "CompletedProgress Task", description: "It's just an CompletedProgress task", isCompleted: true)]
    
    @State private var allItems: [Task] = [
        Task(title: "InProgress Task", description: "It's just an InProgress task", isCompleted: false),
        Task(title: "CompletedProgress Task", description: "It's just an CompletedProgress task", isCompleted: true)
    ]

    
    @State private var newTaskTitle = ""
    @State private var newTaskDesc = ""
    @State private var showAlert = false
    @State var selectedTask: Task? = nil
    @State var showingSheet = false
    @State var taskStatus: TaskStatus = .inProgress
    var taskStatusTitles = ["All", "In progress", "Completed"]

    
    var body: some View {
        NavigationView {
            VStack {
                Picker("", selection: $taskStatus) {
                    Text(taskStatusTitles[0])
                        .tag(TaskStatus.all)
                    Text(taskStatusTitles[1])
                        .tag(TaskStatus.inProgress)
                    Text(taskStatusTitles[2])
                        .tag(TaskStatus.completed)
                }
                .pickerStyle(.segmented)
                
                if taskStatus == .inProgress {
                    List {
                        ForEach(inProgressItems, id: \.self) { task in
                            Button(action: {
                                self.selectedTask = task
                                self.showingSheet = true
                            }) {
                                ListRowView(task: task, isCompleted: false)
                            }
                        }
                    }
                }else if taskStatus == .completed{
                    List {
                        ForEach(completedItems, id: \.self) { task in
                            ListRowView(task: task, isCompleted: true)
                        }
                    }
                }else {
                    List {
                        ForEach(allItems, id: \.self) { task in
                            ListRowView(task: task, isCompleted: false)
                        }
                    }
                }
            }
            .navigationTitle("Task Management")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Add") {
                        showAlert.toggle()
                    }
                    .alert("Add a new task", isPresented: $showAlert) {
                        TextField("Title", text: $newTaskTitle)
                            .textInputAutocapitalization(.never)
                        TextField("Description", text: $newTaskDesc)
                            .textInputAutocapitalization(.never)
                        Button("Add", action: saveNewTask)
                        Button("Cancel", role: .cancel) { }
                    } message: {
                        Text("Please describe your new task.")
                    }
                }
            }
        }
        .confirmationDialog("Title", isPresented: $showingSheet) {
            Button("Mark as Completed") {
                guard let selectedTask = selectedTask else {return}
                inProgressItems = inProgressItems.filter {$0 != selectedTask}
                allItems = allItems.filter {$0 != selectedTask}
                let completedTask = Task(title: selectedTask.title,
                                         description: selectedTask.description,
                                         isCompleted: true)
                completedItems.append(completedTask)
                allItems.append(completedTask)
            }
        }
    }
    
    func saveNewTask() {
        if newTaskTitle.count > 0 && newTaskDesc.count > 0 {
            let newTask = Task(
                title: newTaskTitle,
                description: newTaskDesc,
                isCompleted: false
            )
            inProgressItems.append(newTask)
            allItems.append(newTask)
            newTaskDesc = ""
            newTaskTitle = ""
        }
    }

}

struct ListViews_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
