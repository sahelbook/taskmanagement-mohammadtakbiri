//
//  ListViews.swift
//  TaskManagement-MohammadTakbiri
//
//  Created by Mohammad Takbiri on 4/18/23.
//

import Foundation
import SwiftUI

struct Task : Hashable {
    
    let title : String
    let description : String
    let isCompleted : Bool
}
